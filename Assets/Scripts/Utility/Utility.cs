﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Layer {
    Walkable=8,
    Enemy=9,
    Block=10,
    RaycastEndStop = -1
}

