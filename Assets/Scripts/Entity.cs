﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for anything that posesses in game attributes.
/// </summary>
public class Entity : MonoBehaviour {

    [SerializeField] protected float _maxHealth = 100;

    protected float _currentHealth;

    public float healthAsPercentage
    {
        get
        {
            return _currentHealth / _maxHealth;
        }
    }

    public float currentHealth
    {
        get {
            return _currentHealth;
        }
        set
        {
            _currentHealth = Mathf.Clamp(value, 0, _maxHealth);
        }
    }

    public float maxHealth
    {
        get
        {
            return _maxHealth;
        }
    }

    public bool alive
    {
        get
        {
            return _currentHealth > 0;
        }
    }

    private void Start()
    {
        Init();
    }

    protected virtual void Init()
    {
        currentHealth = maxHealth;
    }
}
