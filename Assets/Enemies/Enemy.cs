﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Entity {

    Player player;
	
	void Start () {
        Init();
        player = FindObjectOfType<Player>();
	}

    protected override void Init()
    {
        base.Init();
    }

    private void Update()
    {
        transform.LookAt(player.transform.position);
    }

}
