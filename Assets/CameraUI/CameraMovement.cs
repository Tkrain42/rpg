﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class CameraMovement : MonoBehaviour {
    
    GameObject player;
    float rotation = 0;
	
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void LateUpdate () {
        if (player)
        {
            transform.position = player.transform.position;
            rotation= CrossPlatformInputManager.GetAxis("Lookaround");
            transform.Rotate(Vector3.up, rotation);
            float scale = CrossPlatformInputManager.GetAxis("Zoom") * .65f+ 1;
            transform.localScale = new Vector3(scale, scale, scale);
        }
	}

    
}
