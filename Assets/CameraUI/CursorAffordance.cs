﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorAffordance : MonoBehaviour {

    [SerializeField]
    Texture2D walkCursor = null;
    [SerializeField]
    Texture2D attackCursor = null;
    [SerializeField]
    Texture2D unknownCursor = null;
    [SerializeField]
    Vector2 cursorHotSpot = new Vector2(0,0);

    CameraRaycaster cameraRaycaster;
	// Use this for initialization
	void Start () {
        cameraRaycaster = GetComponent<CameraRaycaster>();
        cameraRaycaster.onLayerChange += OnLayerChange;
        cameraRaycaster.onTargetChange += OnTargetChange;
	}
	

	// Update is called once per frame
	void OnLayerChange (Layer layer) {
        switch (layer)
        {
            case Layer.Walkable:
                Cursor.SetCursor(walkCursor, cursorHotSpot, CursorMode.Auto);
                break;
            case Layer.Enemy:
                Cursor.SetCursor(attackCursor, cursorHotSpot, CursorMode.Auto);
                break;
            default:
                Cursor.SetCursor(unknownCursor, cursorHotSpot, CursorMode.Auto);
                break;
        }
	}

    Animator lastAnimator;
    void OnTargetChange(RaycastHit hit)
    {
        if (lastAnimator)
        {
            lastAnimator.SetBool("Hover", false);
        }
        Enemy enemy = hit.transform.GetComponent<Enemy>();
        if (enemy)
        {
            var animator = enemy.GetComponent<Animator>();
            if (animator)
            {
                animator.SetBool("Hover", true);
                lastAnimator = animator;
            }
        }
    }

}
