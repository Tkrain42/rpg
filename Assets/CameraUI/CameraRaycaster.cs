﻿using System.Collections.Generic;
using UnityEngine;

public class CameraRaycaster : MonoBehaviour
{

    public Layer[] layerPriorities = {
        Layer.Block,
        Layer.Enemy,
        Layer.Walkable
        
    };

    
    

    public delegate void OnLayerChange(Layer layer);
    public event OnLayerChange onLayerChange;

    public delegate void OnTargetChange(RaycastHit hit);
    public event OnTargetChange onTargetChange;

    Layer lastLayer = Layer.RaycastEndStop;
    Transform lastTransform;

    float distanceToBackground = 100f;
    Camera viewCamera;

    RaycastHit m_hit;

    public RaycastHit hit
    {
        get { return m_hit; }
    }

    Layer m_layerHit;
    public Layer layerHit
    {
        get { return m_layerHit; }
    }

    void Awake() 
    {
        viewCamera = Camera.main;
    }

    void LateUpdate()
    {
        // Look for and return priority layer hit
        foreach (Layer layer in layerPriorities)
        {
            var hit = RaycastForLayer(layer);
            if (hit.HasValue)
            {
                m_hit = hit.Value;
                m_layerHit = layer;
                NotifyIfNewTarget();
                NotifyIfNewLayerHit();
                return;
            }
        }

        // Otherwise return background hit
        m_hit.distance = distanceToBackground;
        m_layerHit = Layer.RaycastEndStop;
        NotifyIfNewLayerHit();
    }

    private void NotifyIfNewTarget()
    {
        if (m_hit.transform != lastTransform)
        {
            lastTransform = m_hit.transform;
            if(onTargetChange!=null)
            {
                onTargetChange(m_hit);
            }
        }
    }

    private void NotifyIfNewLayerHit()
    {
        if (m_layerHit != lastLayer)
        {
            lastLayer = m_layerHit;
            if(onLayerChange!=null)
            {
                onLayerChange(m_layerHit);
            }
        }
    }

    RaycastHit? RaycastForLayer(Layer layer)
    {
        int layerMask = 1 << (int)layer; // See Unity docs for mask formation
        Ray ray = viewCamera.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit; // used as an out parameter
        bool hasHit = Physics.Raycast(ray, out hit, distanceToBackground, layerMask);
        if (hasHit)
        {
            return hit;
        }
        return null;
    }
}
