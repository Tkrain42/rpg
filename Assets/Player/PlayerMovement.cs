using System;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityStandardAssets.CrossPlatformInput;

[RequireComponent(typeof (ThirdPersonCharacter))]
public class PlayerMovement : MonoBehaviour
{
    [Range(0,10)]
    [SerializeField] float attackStopMoveRadius = 1.5f;

    ThirdPersonCharacter m_Character;   // A reference to the ThirdPersonCharacter on the object
    CameraRaycaster cameraRaycaster;
    Vector3 currentClickTarget;
    Vector3 attackTargetLocation;
    private Transform m_Cam;                  // A reference to the main camera in the scenes transform
    private Vector3 m_CamForward;             // The current forward direction of the camera
    private Vector3 m_Move;
    private bool m_Jump;

    private void Start()
    {
        cameraRaycaster = Camera.main.GetComponent<CameraRaycaster>();
        m_Cam = Camera.main.transform;
        m_Character = GetComponent<ThirdPersonCharacter>();
        currentClickTarget = transform.position;
    }

    // Fixed update is called in sync with physics
    private void FixedUpdate()
    {
        if (Input.GetMouseButton(0))
        {
            ProcessMouse();

        }
        else
        {
            ProcessCrossPlatformInput();
        }

    }

    private void ProcessCrossPlatformInput()
    {
        
        // read inputs
        float h = CrossPlatformInputManager.GetAxis("Horizontal");
        float v = CrossPlatformInputManager.GetAxis("Vertical");
        bool crouch = Input.GetKey(KeyCode.C);

        // calculate move direction to pass to character
        if (m_Cam != null)
        {
            // calculate camera relative direction to move:
            m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
            m_Move = v * m_CamForward + h * m_Cam.right;
        }
        else
        {
            // we use world-relative directions in the case of no main camera
            m_Move = v * Vector3.forward + h * Vector3.right;
        }
#if !MOBILE_INPUT
        // walk speed multiplier
        if (Input.GetKey(KeyCode.LeftShift))
            m_Move *= 0.5f;
#endif

        // pass all parameters to the character control script
        m_Character.Move(m_Move, crouch, m_Jump);
        m_Jump = false;
        currentClickTarget = transform.position;
    }

    private void ProcessMouse()
    {
        switch (cameraRaycaster.layerHit)
        {
            case Layer.Walkable:
                currentClickTarget = cameraRaycaster.hit.point;
                Vector3 movement = currentClickTarget-transform.position;
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    movement *= 0.5f;
                }
                movement *= .25f;
                attackTargetLocation = currentClickTarget;
                m_Character.Move(movement, false, false);
                break;
            case Layer.Enemy: //TODO Fix enemy attacking
                currentClickTarget = cameraRaycaster.hit.point;
                movement = currentClickTarget - transform.position;
                if (movement.magnitude > attackStopMoveRadius)
                {
                    movement -= movement.normalized * attackStopMoveRadius;
                    attackTargetLocation = transform.position + movement;
                    m_Character.Move(movement, false, false);
                }
                break;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawLine(transform.position, currentClickTarget);
        Gizmos.DrawSphere(currentClickTarget, .1f);
        Gizmos.DrawSphere(attackTargetLocation, .1f);
        Gizmos.DrawWireSphere(transform.position, attackStopMoveRadius);
    }
}

